﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Pinger pinger = new Pinger();
            pinger.Run();
        }
    }
}
