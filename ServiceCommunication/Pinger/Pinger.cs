﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using ServiceCommunication;
using System.Threading;

namespace Pinger
{
    class Pinger
    {
        RabbitMQWrapper _client = new RabbitMQWrapper();
        string exchangeName = "PingPongExhange";
        string listenQueueName = "ping_queue";
        string writeQueueName = "pong_queue";
        string listenRoutingKey = "ping";
        string writeRoutingKey = "pong";
        public Pinger()
        {
            _client.DeclareExhange(exchangeName, ExchangeType.Direct);
            _client.BindQueue(exchangeName, listenRoutingKey, listenQueueName);
            _client.BindQueue(exchangeName, writeRoutingKey, writeQueueName);

            _client.ListenQueue(listenQueueName, MessageRecieved);
        }

        public void Run()
        {
            SendMessage(exchangeName, writeRoutingKey, writeQueueName, "ping");
            while (true) { }
        }

        public void MessageRecieved(object sender, BasicDeliverEventArgs args)
        {
            bool processed = false;
            try
            {
                var message = Encoding.UTF8.GetString(args.Body.ToArray());
                Console.WriteLine();
                Console.WriteLine("Datetime: " + DateTime.Now);
                Console.WriteLine("MessageRecieved: " + message);
                Console.WriteLine();

                Thread.Sleep(2500);
                SendMessage(exchangeName, writeRoutingKey, writeQueueName, "ping ");
                processed = true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                processed = false;
            }
            finally
            {
                _client.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        public void SendMessage(string exchangeName, string routingKey, string queueName, string message)
        {
            _client.SendMessageToQueue(exchangeName, routingKey, queueName, message);
        }
    }
}
