﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Ponger ponger = new Ponger();
            ponger.Run();
        }
    }
}
