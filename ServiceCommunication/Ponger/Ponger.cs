﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceCommunication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Ponger
{
    class Ponger
    {
        RabbitMQWrapper _client = new RabbitMQWrapper();
        string exchangeName = "PingPongExhange";
        string listenQueueName = "pong_queue";
        string writeQueueName = "ping_queue";
        string listenRoutingKey = "pong";
        string writeRoutingKey = "ping";
        public Ponger()
        {
            _client.DeclareExhange(exchangeName, ExchangeType.Direct);
            _client.BindQueue(exchangeName, listenRoutingKey, listenQueueName);
            _client.BindQueue(exchangeName, writeRoutingKey, writeQueueName);

            _client.ListenQueue(listenQueueName, MessageRecieved);
        }

        public void Run()
        {
            while (true) { }
        }

        public void MessageRecieved(object sender, BasicDeliverEventArgs args)
        {
            bool processed = false;
            try
            {
                  var message = Encoding.UTF8.GetString(args.Body.ToArray());
                Console.WriteLine();
                Console.WriteLine("Datetime: " + DateTime.Now);
                Console.WriteLine("MessageRecieved: " + message);
                Console.WriteLine();

                Thread.Sleep(2500);
                SendMessage(exchangeName, writeRoutingKey, writeQueueName, "pong");

                processed = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                processed = false;
            }
            finally
            {
                _client.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        public void SendMessage(string exchangeName, string routingKey, string queueName, string message)
        {
            _client.SendMessageToQueue(exchangeName, routingKey, queueName, message);
        }
    }
}
